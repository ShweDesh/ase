============================================================================
ASE Workshop: Software Development for Atomic Scale Modeling - Chalmers 2019
============================================================================

.. note::

    Up-to-date information is on `the workshop page <https://ase-workshop.materialsmodeling.org>`__.

The ASE Workshop will be held at `Chalmers University of Technology <https://www.chalmers.se/en>`__ in `Gothenburg, Sweden <https://www.goteborg.com/en>`__ from **November 19-22, 2019**.


Program
=======

See program on `the workshop page <https://ase-workshop.materialsmodeling.org>`__.


Registration
============

Registration is closed.


Organizers
==========

* Paul Erhart, Chalmers University of Technology, Sweden
* Tuomas Rossi, Chalmers University of Technology, Sweden
* Ask Hjorth Larsen, Simune Atomistics S.L., Spain
* Jens Jørgen Mortensen, Technical University of Denmark, Denmark
* Kristian Sommer Thygesen, Technical University of Denmark, Denmark

For questions, please contact the organizers at ase-workshop@materialsmodeling.org.
